<?php

declare(strict_types=1);

namespace CarogaNET\Razr\DependencyInjection;

use CarogaNET\Razr\Exceptions\DependencyExceptions;
use Psr\Container\ContainerInterface;

final class Container extends \ArrayObject implements ContainerInterface
{
    public function set(string $classReference): void
    {
        $this[$classReference::$name] = new ContainerEntry($classReference::$name, $classReference::$description, $classReference);
    }

    public function has($name): bool
    {
        return isset($this[$name]);
    }

    public function get($name)
    {
        if (!$this->has($name)) {
            throw DependencyExceptions::NotRegistered($name);
        }
        $classReference = $this[$name]->getClassReference();

        return new $classReference();
    }

    public function loadNamespaceMap(array $namespaceMap)
    {
        foreach ($namespaceMap as $classReference) {
            // @todo check if namespace isn't already registered, throw exception if it was.
            $this->set($classReference);
        }
    }
}
