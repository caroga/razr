<?php

declare(strict_types=1);

namespace CarogaNET\Razr\Exceptions;

/**
 * Class AppExceptions
 * @package CarogaNET\Razr\Exceptions
 */
class AppExceptions extends \Exception
{
    public static function NotOnCliEnvironment()
    {
        return new self('Application can only be run on CLI.');
    }
}
