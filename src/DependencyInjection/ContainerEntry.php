<?php

declare(strict_types=1);

namespace CarogaNET\Razr\DependencyInjection;

final class ContainerEntry
{
    private string $name;
    private string $description;
    private string $classReference;

    public function __construct(string $name, string $description, string $classReference)
    {
        $this->name = $name;
        $this->description = $description;
        $this->classReference = $classReference;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getClassReference(): string
    {
        return $this->classReference;
    }
}
