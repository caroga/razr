<?php

declare(strict_types=1);

namespace CarogaNET\Razr\Loaders;

use CarogaNET\Razr\Exceptions\NamespaceLoaderExceptions;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class NamespaceLoaderTest extends TestCase
{
    use ProphecyTrait;

    public function testLoad()
    {
        $expectedArray = [
            'CarogaNET\Razr\Commands\Command',
            'CarogaNET\Razr\Commands\DefaultCommand',
        ];

        arsort($expectedArray);

        $loader = new NamespaceLoader();
        $path = __DIR__ . '/../../../../src/Commands';
        $map = $loader->createNamespaceMap($path, '*Command.php');

        $this->assertIsArray($map);
        $this->assertSame($expectedArray, $map);
    }

    public function testLoadWithoutNamespaceInFiles()
    {
        $loader = new NamespaceLoader();
        $path = __DIR__ . '/../../../../';
        self::expectException(NamespaceLoaderExceptions::class);
        $loader->createNamespaceMap($path, '*.yaml');
    }
}
