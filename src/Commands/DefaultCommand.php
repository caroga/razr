<?php
declare(strict_types=1);

namespace CarogaNET\Razr\Commands;

use CarogaNET\Razr\DependencyInjection\ContainerEntry;

class DefaultCommand extends Command
{
    public static string $name = 'razr:help';
    public static string $description = 'Shows the list of current capabilities.';

    private function logo(): void
    {
        $text = '    __________                        
    \______   \_____  _______________ 
     |       _/\__  \ \___   /\_  __ \
     |    |   \ / __ \_/    /  |  | \/
     |____|_  /(____  /_____ \ |__|   
            \/      \/      \/        ';
        $this->climate->yellow($text);
        $this->climate->br(2);
        $this->climate->tab()->green('Hi, and thank you for using Razr!');
        $this->climate->tab()->dim()->underline()->out('Currently I am still undergoing development. So please excuse me if I don\'t understand everything properly yet.');
        $this->climate->br(2);
    }

    public function execute(array $args = []): void
    {
        $this->climate->clear();
        $this->logo();

        $this->climate->tab()->out('Available commands:');
        $this->climate->br();
        $data = array_map(function (ContainerEntry $entry) {
            return [$entry->getName(), $entry->getDescription()];
        }, $args['commandContainer']);

        $this->climate->tab()->dim()->out('Name, Description');
        $this->climate->table($data, '        ');
        $this->climate->br();
    }
}
