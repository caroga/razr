<?php
declare(strict_types=1);

namespace CarogaNET\Razr\Commands;

use PHPUnit\Framework\TestCase;

class CommandTest extends TestCase
{
    private CommandInterface $anonymousCommand;

    public function setUp(): void
    {
        $this->anonymousCommand = new class extends Command {
            public static string $name = 'razr:some:name';
            public static string $description = 'My description';

            public function execute(array $args = [])
            {
                // TODO: Implement execute() method.
            }
        };
    }

    public function testClassHasNecessaryAttributes()
    {
        $this->assertClassHasAttribute('name', Command::class);
        $this->assertClassHasAttribute('description', Command::class);
    }

    public function testName()
    {
        $this->assertEquals('razr:some:name', $this->anonymousCommand::$name);
    }

    public function testDescription()
    {
        $this->assertEquals('My description', $this->anonymousCommand::$description);
    }

    public function test__toString()
    {
        $this->assertEquals('CarogaNET\Razr\Commands\Command', (string)$this->anonymousCommand);
    }

}
