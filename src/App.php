<?php

declare(strict_types=1);

namespace CarogaNET\Razr;

use CarogaNET\Razr\DependencyInjection\Container;
use CarogaNET\Razr\Commands\DefaultCommand;
use CarogaNET\Razr\Exceptions\AppExceptions;
use CarogaNET\Razr\Loaders\NamespaceLoader;

final class App
{
    private Container $commandContainer;
    private NamespaceLoader $loader;

    public function __construct(string $sapiName)
    {
        if ($sapiName !== 'cli') {
            /** @noinspection PhpUnhandledExceptionInspection */
            throw AppExceptions::NotOnCliEnvironment();
        }

        $this->loader = new NamespaceLoader();
        $this->commandContainer = new Container();
    }

    public function commandContainer()
    {
        return $this->commandContainer;
    }

    public function autoLoadCommands(string $path = ''): void
    {
        if (!empty($path)) {
            $namespaceMap = $this->loader->createNamespaceMap($path, '*Command.php');
        }
        $namespaceMap[] = 'CarogaNET\Razr\Commands\DefaultCommand';
        $this->commandContainer->loadNamespaceMap($namespaceMap);
    }

    public function runCommand(array $argv): int
    {
        if (empty((array)$this->commandContainer)) {
            $this->autoLoadCommands();
        }

        if (!isset($argv[1])) {
            $command = DefaultCommand::$name;
            $parameters['commandContainer'] = (array)$this->commandContainer;
        } else {
            $command = $argv[1];
        }

        array_shift($argv);
        $parameters['cli_args'] = $argv;

        $this->commandContainer->get($command)->execute($parameters);

        return 0;
    }
}
