<?php

declare(strict_types=1);

namespace CarogaNET\Razr\Exceptions;

use Psr\Container\NotFoundExceptionInterface;

/**
 * Class CommandExceptions
 * @package CarogaNET\Razr\Exceptions
 */
class DependencyExceptions extends \Exception implements NotFoundExceptionInterface
{
    /**
     * @param $command
     * @throws DependencyExceptions
     */
    public static function NotRegistered($command)
    {
        throw new self("Command {$command} is not registered.");
    }
}
