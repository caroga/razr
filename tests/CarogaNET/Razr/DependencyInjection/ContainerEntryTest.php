<?php
declare(strict_types=1);

namespace CarogaNET\Razr\DependencyInjection;

use CarogaNET\Razr\Commands\CommandInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class ContainerEntryTest extends TestCase
{
    use ProphecyTrait;
    private ContainerEntry $entry;
    private string $name = 'foo';
    private string $description = 'bar';

    public function setUp(): void
    {
        $this->entry = new ContainerEntry($this->name, $this->description, CommandInterface::class);
    }

    public function test__construct()
    {
        $this->assertInstanceOf(ContainerEntry::class, $this->entry);
    }

    public function tearDown(): void
    {
        unset($this->entry);
    }

    public function testGetName()
    {
        $this->assertIsString($this->entry->getName());
        $this->assertSame($this->name, $this->entry->getName());
    }

    public function testGetClassReference()
    {
        $this->assertIsString($this->entry->getClassReference());
        $this->assertSame(CommandInterface::class, $this->entry->getClassReference());
    }

    public function testGetDescription()
    {
        $this->assertIsString($this->entry->getDescription());
        $this->assertSame($this->description, $this->entry->getDescription());
    }
}
