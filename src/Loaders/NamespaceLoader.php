<?php

declare(strict_types=1);

namespace CarogaNET\Razr\Loaders;

use CarogaNET\Razr\Exceptions\NamespaceLoaderExceptions;

final class NamespaceLoader extends \ArrayObject
{
    public function createNamespaceMap(string $path, string $pattern = '*.php'): array
    {
        $suffix = null;
        if ($pattern !== '*.php') {
            $suffix = str_replace(['*', '.php'], '', $pattern);
        }

        foreach (glob($path . '/' . $pattern) as $fileReference) {
            $this->addToMap($fileReference, $suffix);
        }
        $namespaceMap = array_values((array)$this);
        arsort($namespaceMap);

        return $namespaceMap;
    }

    protected function addToMap($fileReference): void
    {
        $basename = basename($fileReference);
        $classReference = str_replace('.php', '', $basename);
        $fullNamespace = sprintf("%s\\%s", $this->getNamespace($fileReference), $classReference);

        $this[$fileReference] = $fullNamespace;
    }

    /**
     * @param $fileReference
     * @return mixed
     * @throws NamespaceLoaderExceptions
     */
    protected function getNamespace($fileReference): string
    {
        $namespace = preg_grep('/^namespace /', file($fileReference));

        if (empty($namespace)) {
            throw NamespaceLoaderExceptions::FileDoesNotContainANamespace($fileReference);
        }
        $namespaceLine = [];
        preg_match('/^namespace (.*);$/', end($namespace), $namespaceLine);

        return end($namespaceLine);
    }
}
