<?php
declare(strict_types=1);

namespace CarogaNET\Razr;

use CarogaNET\Razr\DependencyInjection\Container;
use CarogaNET\Razr\Commands\DefaultCommand;
use CarogaNET\Razr\Exceptions\AppExceptions;
use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
    private App $app;

    public function setUp(): void
    {
        $this->app = new App(php_sapi_name());
    }

    public function testConstructFails()
    {
        self::expectException(AppExceptions::class);
        $foo = new App('bar');
        unset($foo);
    }

    public function testAutoLoadCommands()
    {
        $this->app->autoLoadCommands(__DIR__ . '/../src/Commands');
        $this->assertArrayHasKey(DefaultCommand::$name, (array)$this->app->commandContainer());
    }

    public function testHasCommandContainer()
    {
        $this->assertInstanceOf(Container::class, $this->app->commandContainer());
    }
}
